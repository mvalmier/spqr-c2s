#--------HOW TO USE THIS SCRIPT--------
#lancer le script avec les droits root pour sauvegarder les bases et les pousser vers le git
#--------HOW IT'S WORK---------
#il recupere les conteneurs qui contient la chaine de caractere "testpostgre"
#il filtre le resultat pour recuperer les noms des conteneurs
#Il sauvegarde les bases de donnes sur chaque conteneur avec pg_dump
#il sauvegarde les filestore depuis l'url 
#mettre a jour le fichier domain dans le repertoire /root/sql/ pour ajouter de nouveaux domaines 

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games
FULLDATE=`date '+%Y-%m-%d %H:%M:%S'`
cd /root/sql/
rm listdocker namedocker ip.txt
docker ps -a | grep "TestPostGre" > listdocker
awk '{print$1}' listdocker > namedocker
file1=$(<namedocker)
for line in $file1
do
	if [ ! -d $line ]; then
		mkdir $line

	fi
	docker exec -t -u postgres $line pg_dumpall -c > "/root/sql/$line/dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql"
	find "/root/sql/$line/" -mtime +20 -exec rm {} \;
done
#while read line1
#do
#	echo $line1
#	docker exec -i $line1 bash -c  "ip add | grep global"
#done < namedocker
spqrserver=$(<domain)
for line2 in $spqrserver
do
	if [ ! -d $line2 ]; then
                mkdir $line2

        fi
	#curl --data "master_pwd=tlabtlab&name=spqrdb&backup_format=zip" -LOk "https://$line2/web/database/backup" > "/root/sql/$line2/dump_`date +%d-%m-%Y"_"%H_%M_%S`.zip"
	#curl -X POST -F 'master_pwd=tlabtlab' -F 'name=spqrdb' -F 'backup_format=zip' "https://$line2/web/database/backup" > "/root/sql/$line2/dump_`date +%d-%m-%Y"_"%H_%M_%S`.zip"
	curl -X POST -F 'master_pwd=tlabtlab' -F 'name=spqrdb' -F 'backup_format=zip' "https://$line2/web/database/backup" -k > "/root/sql/$line2/dump_`date +%d-%m-%Y"_"%H_%M_%S`.zip"
	find "/root/sql/$line2/" -mtime +20 -exec rm {} \;
done
cd /root/sql/
git add *
git commit -m "Automatic backup - $FULLDATE"
git push -u origin master
