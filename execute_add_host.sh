#!/bin/bash

# $1 = Nom de la stack
# $2 = Hostname
# $3 = IP à mettre entre "
# $4 = Postgres ou non ? y=Oui n=Non
# $5 = Odoo ou non ? y=Oui n=Non
# $6 = id machine à superviser

# Exécution du addhost.sh sur le nagios

docker exec  -it b8a277fff1c0  /usr/local/nagios/etc/servers/addhost.sh $1 $2 $3 $4 $5 $6

# Update des sources de la machine à superviser

docker exec -it $6 yes | apt-get update

# Installation agent NRPE et plugins 

docker exec -it $6 apt-get install  nagios-nrpe-server nagios-plugins

# Création si base postgres utilisateur nagios avec PSQL

if [ $4 = "y" ]

then

	docker exec -it $6 su postgres bash -c "psql -c \"CREATE USER nagios WITH PASSWORD 'nagios';\""
fi

# Redémarrage de nagios 

docker exec -it b8a277fff1c0 service nagios restart
