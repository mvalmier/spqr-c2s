#!/bin/bash

# $1=Nom de la stack
# $2=Hostname 
# $3=IP a mettre entre "
# $4= Postgres ou non ? y=Oui n=Non 
# $5= Odoo ou non ? y=oui n=Non 

#members=$(grep members /usr/local/nagios/etc/servers/$1.cfg)
#variable=$(cat /usr/local/nagios/etc/servers/$1.cfg | head -15 | tail -1)


# Creation du fichier de configuration

if [ ! -f /usr/local/nagios/etc/servers/$1.cfg ]

then

	touch /usr/local/nagios/etc/servers/$1.cfg

fi

# Declaration du host dans le fichier de configuration

echo -e "\ndefine host{\n        use                     generic-host\n        host_name               $2\n        alias                   $1\n        address                 $3\n        max_check_attempts              5\n        contact_groups          admins\n	}\n"  >>/usr/local/nagios/etc/servers/$1.cfg

# Declaraton du  hostgroup
if grep -q  "define hostgroup" /usr/local/nagios/etc/servers/$1.cfg

then

	#creation d une variable qui a pour contenu la ligne 15 qui comprend les membres du hostgroup
	members=$(sed -n 15p /usr/local/nagios/etc/servers/$1.cfg)
	echo $members

	#creation  une autre variable qui assemble la ligne existante avec le host à ajouter
	test="$members,$2"

	#remplacement de la ligne 15 par la variable test
	sed -ie "15s/.*/$test/" /usr/local/nagios/etc/servers/$1.cfg
	final=$(sed -n 15p /usr/local/nagios/etc/servers/$1.cfg)
	echo $final

else

	echo -e "\ndefine hostgroup{\n        hostgroup_name  $1\n        alias           $1\n        members         $2\n        }\n" >> /usr/local/nagios/etc/servers/$1.cfg

fi

# Declaration du service group

if  [ $4 = "y" ]

then

	echo -e "\ndefine servicegroup{\n			servicegroup_name       $1Postgres\n			alias                   Postgres Services\n			members                 $2,PING,$2,check_postgresql_connection\n			}\n" >> /usr/local/nagios/etc/servers/$1.cfg

fi

if [ $5 = "y" ]

then

	echo -e "\ndefine servicegroup{\n			servicegroup_name       $1Odoo\n			alias                   Odoo Services\n			members                 $2,PING,$2,HTTPS\n			}\n" >> /usr/local/nagios/etc/servers/$1.cfg

fi

# Ajout des services a superviser 

echo -e "\ndefine service {\n        use                             generic-service\n        host_name                       $2\n        service_description             PING\n        check_command                   check_ping!100.0,20%!500.0,60%\n        contact_groups                  guest,admins\n	}\n" >> /usr/local/nagios/etc/servers/$1.cfg

# Si la machine √† superviser poss√®de une base postgr√®s √† superviser ajouter les services plugin postgresql

if [ $4 = "y" ]

then 

	echo -e "\ndefine service {\n        service_description             check_postgresql_connection\n        use                             generic-service\n        host_name                       $2\n        check_command                   check_postgresql_connection\n        contact_groups                  guest,admins\n	}\n" >> /usr/local/nagios/etc/servers/$1.cfg

fi

if [ $4 = "n" ]

then
	echo "No Postgres Database on host"

fi

# Si Odoo  present ajout service https

if [ $5 = "y" ]

then

	echo -e "\ndefine service {\n        use                             generic-service\n        host_name                       $2\n        service_description             HTTPS\n        check_command                   check_http\n        notifications_enabled           0\n        contact_groups                  guest,admins\n 	}\n" >> /usr/local/nagios/etc/servers/$1.cfg

fi

if [ $5 = "n" ]

then

	echo "No Odoo on host"

fi

